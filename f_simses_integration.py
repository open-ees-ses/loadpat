import csv
from configparser import ConfigParser
from simses.main import SimSES
import pandas as pd
import os

from simses.commons.config.simulation.general import GeneralSimulationConfig


class SimSES_Integration:
    """
        SimSES Integration builds the basic config
    """
    def __init__( self, result_path: str, storage_id: str, file, simulation_config: ConfigParser = None):
        self.__simses = None
        self.__simulation_name = None
        self.__simulation_config: ConfigParser = ConfigParser()
        self.__general_config = GeneralSimulationConfig(simulation_config)
        self.__analysis_config: ConfigParser = ConfigParser()
        self.__result_path = result_path
        self.__storage_id = storage_id
        self.__file = file
        self.__timestep = None
        self.__loadprofile = pd.read_csv(file.name)

        self.__DATE_FORMATS: [str] = ['%Y-%m-%d %H:%M:%S', '%Y-%m-%d %H:%M', '%d.%m.%Y %H:%M:%S', '%d.%m.%Y %H:%M',
                             'epoch']

        if len(self.__loadprofile.columns) == 2:
            self.__loadprofile = self.__loadprofile.set_index(self.__loadprofile.columns[0])

        time = self.__loadprofile.index[1]
        date_format_known = 0
        for date_format in self.__DATE_FORMATS:
            try:
                if date_format == 'epoch':
                    float(time)
                    self.__timeindex = pd.to_datetime(self.__loadprofile.index, unit='s')
                    date_format_known = 1
                else:
                    pd.to_datetime(time, format=date_format)
                    self.__timeindex = pd.to_datetime(self.__loadprofile.index, format=date_format)
                    date_format_known = 1
            except ValueError:
                pass

        if date_format_known == 0:
            raise Exception('Unknown date format for ' + time)

    def set_up_simses_simulation(self, simulation_name: str) -> None:
        self.__simulation_name = simulation_name
        self.__simses: SimSES = SimSES(str(self.__result_path + '\\').replace('\\', '/'), simulation_name,
                                   do_simulation=True,
                                   do_analysis=True,
                                   simulation_config=self.__simulation_config,
                                   analysis_config=self.__analysis_config)

    def build_general_config(self, resolution: float) -> None:

        self.__simulation_config.add_section('GENERAL')
        self.__timestep = self.__timeindex[2] - self.__timeindex[1]
        self.__simulation_config.set('GENERAL', 'TIME_STEP', str(resolution))
        self.__simulation_config.set('GENERAL', 'START', str(self.__timeindex[0]))
        self.__simulation_config.set('GENERAL', 'END', str(self.__timeindex[-1]))

    def build_analysis_config(self) -> None:

        self.__analysis_config.add_section('GENERAL')
        self.__analysis_config.set('GENERAL', 'PLOTTING', 'False')
        self.__analysis_config.set('GENERAL', 'MERGE_ANALYSIS', 'False')

    def build_ems_config(self, simses_parameter) -> None:
        self.__simulation_config.add_section('ENERGY_MANAGEMENT')
        self.__simulation_config.set('ENERGY_MANAGEMENT', 'STRATEGY', simses_parameter["ems"])
        self.__simulation_config.set('ENERGY_MANAGEMENT', 'MULTI_USE_STRATEGIES', str('SimplePeakShaving, ResidentialPvGreedy'))
        self.__simulation_config.set('ENERGY_MANAGEMENT', 'ENERGY_ALLOCATION', '0.5, 0.5')
        self.__simulation_config.set('ENERGY_MANAGEMENT', 'POWER_ALLOCATION', '0.5, 0.5')
        self.__simulation_config.set('ENERGY_MANAGEMENT', 'RANKING', '1, 2')

        self.__simulation_config.set('ENERGY_MANAGEMENT', 'Max_Power', str(simses_parameter["peak_shaving_limit"]))
        print("Peak-Shaving Limit: " + str(simses_parameter["peak_shaving_limit"]) + "W")

    def build_storage_system_config(self, max_power: float, energy: float, cell_type: str, start_soc: float) -> None:
        self.__simulation_config.add_section('STORAGE_SYSTEM')
        self.__simulation_config.set('STORAGE_SYSTEM', 'STORAGE_SYSTEM_AC', 'system_1,' + str(abs(max_power)) + ',600,'
                                                                         'acdc,no_housing,no_hvac')
        self.__simulation_config.set('STORAGE_SYSTEM', 'ACDC_CONVERTER', 'acdc, NottonAcDcConverter, 1')
        # SimSES works with Wh
        self.__simulation_config.set('STORAGE_SYSTEM', 'STORAGE_TECHNOLOGY',
                                     'storage_1, ' + str(energy) + ', lithium_ion,'
                                     + cell_type)
        self.__simulation_config.set('STORAGE_SYSTEM', 'HOUSING', 'no_housing, NoHousing')
        self.__simulation_config.set('STORAGE_SYSTEM', 'SOLAR_IRRADIATION_MODEL', 'NoSolarIrradiationModel')
        self.__simulation_config.set('STORAGE_SYSTEM', 'THERMAL_SIMULATION', 'True')
        self.__simulation_config.add_section('BATTERY')
        self.__simulation_config.set('BATTERY', 'START_SOC', str(start_soc))
        #self.__simulation_config.set('BATTERY', 'MIN_SOC', str(self.__storage_config.min_soc))
        #self.__simulation_config.set('BATTERY', 'MAX_SOC', str(self.__storage_config.max_soc))

    def build_profile_config(self, simulation_name, pv_power) -> None:
        self.__simulation_config.add_section('PROFILE')
        self.__simulation_config.set('PROFILE', 'LOAD_PROFILE_SCALING', 'False')
        self.__simulation_config.set('PROFILE', 'LOAD_SCALING_FACTOR', '1')
        if pv_power > 0:
            self.__simulation_config.set('PROFILE', 'GENERATION_PROFILE_SCALING', "Power")
            self.__simulation_config.set('PROFILE', 'GENERATION_SCALING_FACTOR', str(pv_power))

        profile_path = os.path.dirname(self.__result_path)
        profile_path = os.path.dirname(profile_path)
        self.__simulation_config.set('PROFILE', 'POWER_PROFILE_DIR', str(profile_path).replace('\\','/') + '/')
        # self.__simulation_config.set('PROFILE', 'LOAD_PROFILE', self.create_load_profile_csv(timeindex,
        #                                                                                      loadprofile))
        head_tail = os.path.split(self.__file.name)
        if simulation_name=="LoadPAT_Simulation_Original":
            filename = head_tail[1][:-4] + '.csv'
            self.__simulation_config.set('PROFILE', 'LOAD_PROFILE', filename)
        elif simulation_name=="LoadPAT_Simulation_Anonym":
            filename = head_tail[1][:-4] + '_anonym.csv'
            self.__simulation_config.set('PROFILE', 'LOAD_PROFILE', filename)
        else: # Batch simulation name
            self.__simulation_config.set('PROFILE', 'POWER_PROFILE_DIR', str(profile_path).replace('\\', '/') + '/Monte_Carlo_Profiles/')
            self.__simulation_config.set('PROFILE', 'LOAD_PROFILE', simulation_name)

    def create_load_profile_csv(self, timeindex: pd.DatetimeIndex, loadprofile) -> str:
        annual_power = sum(loadprofile)
        with open(os.path.join(self.__result_path, self.__storage_id) + '.csv', 'w', newline='') as csvfile:
            timeseries = csv.writer(csvfile, delimiter=',')
            timeseries.writerow(['# Source: LOADPAT'])
            timeseries.writerow(['# Unit: W'])
            timeseries.writerow(['# Sampling in s: ' + str(self.__timestep.seconds)])
            timeseries.writerow(['# Origin: Timeseries generated in open_BEA'])
            timeseries.writerow(['# Datasets: 1'])
            timeseries.writerow(['# Publishable: Yes'])
            timeseries.writerow(['# Annual load consumption in kWh: ' + str(annual_power)])
            timeseries.writerow('')

            i = 0
            while i < len(loadprofile):
                timeseries.writerow([timeindex.T[i].value / 10 ** 9 + timeindex.freq.delta.seconds, loadprofile])
                i += 1

        return self.__storage_id + '.csv'

    def simulation(self) -> None:
        self.__simses.run_simulation()

    def simulation_one_step(self, time, power) -> float:
        time_simses = time + self.__timestep.seconds
        self.__simses.run_one_simulation_step(time=time_simses, power=power)
        return self.__simses.state.get(self.__simses.state.AC_POWER_DELIVERED), self.__simses.state.get(self.__simses.state.SOC)

    def get_remaining_capacity(self):
        return self.__simses.state.get(self.__simses.state.CAPACITY)

    def analysis(self) -> None:
        self.__simses.run_analysis()

    def get_sim_config(self):
        return self.__simulation_config

    def close(self) -> None:
        self.__simses.close()