#!/usr/bin/env python3

import ast
import tkinter as tk
import pandas as pd
import numpy as np
import os
import ntpath
import matplotlib
from PIL import Image, ImageTk
from tkinter.filedialog import askopenfile
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from f_anonymization import anonymization_function
from f_profile_comparison import profile_comparison
from f_simses_window import SimSES_GUI
matplotlib.use("TkAgg")  # Backend of Matplotlib

def main():
    LoadPAT_GUI()


class LoadPAT_GUI:

    def __init__(self):
        self.root = tk.Tk()  # start elements of GUI
        self.loadprofile = pd.DataFrame
        self.anonym_profile = pd.DataFrame
        self.comparison = dict()
        self.canvas = tk.Canvas(self.root, width=1300, height=900)
        self.canvas.grid(columnspan=8, rowspan=10)
        self.slider1 = tk.Scale()
        self.Normalization = tk.IntVar()  # 1 or 0
        self.analysis_result = str()
        self.save_info = str()
        self.fontsize_labels = 12
        self.file = None
        self.box_threshold = None
        self.act_threshold = None
        self.box_monte_carlo = None
        self.monte_carlo_option = 0
        self.level = 0
        self.file_folder = None

        # Define default parameters:
        self.monte_carlo_default = ["100"]
        self.threshold_default = ["130"]

        self.create_widgets()


    def plot_function(self, data):
        '''This function plots the original and anonymized load profiles within the main GUI.'''

        temp_loadprofile = data["loadprofile"].copy()

        f = Figure(figsize=(3, 2.5), dpi=100)
        a = f.add_subplot(111)
        f.tight_layout(rect=(0.02, 0.03, 1, 0.95))
        a.plot(temp_loadprofile.values)

        threshold = int(self.box_threshold.get())

        if data["original_or_anonym"] == "original":
            mean_value = float(np.nanmean(temp_loadprofile.values))
            line2 = a.axhline(y=mean_value, color='r', linestyle='--')
            threshold_value = float(np.nanmean(temp_loadprofile.values) * threshold/100)
            line3 = a.axhline(y=threshold_value, color='r', linestyle='-')
            a.legend([line3, line2], ['Threshold', 'Average'], fontsize=7)

        f.axes[0].grid(True)
        f.axes[0].set_xlim(0, len(temp_loadprofile))
        f.axes[0].set_ylim([0, np.nanmax(temp_loadprofile.values)*1.1])
        f.supxlabel("Data point", fontsize='small')
        f.supylabel(data["ylabel"], fontsize='small')

        return f

    def create_widgets(self):

        #Pparameters:
        monte_carlo_default = self.monte_carlo_default
        threshold_default = self.threshold_default


        self.root.winfo_toplevel().title("LoadPAT")
        # Logo SimBAS
        logo = Image.open('Simbas.png')
        logo = logo.resize((256, 120))  # The (250, 250) is (height, width)
        logo = ImageTk.PhotoImage(logo)
        logo_label = tk.Label(image=logo)
        logo_label.image = logo

        rowcounter = 0
        row_plots = 4
        rowspan_plots = 3

        logo_label.grid(columnspan=3, column=0, row=rowcounter)

        # Logo LoadPAT
        logo2 = Image.open('LoadPAT Icon TIF.tif')
        logo2 = logo2.resize((156, 100))  # The (250, 250) is (height, width)
        logo2 = ImageTk.PhotoImage(logo2)
        logo_label2 = tk.Label(image=logo2)
        logo_label2.image = logo2

        logo_label2.grid(columnspan=2, column=4, row=rowcounter)

        # Welcome & Instructions
        general_instructions = tk.Label(self.root, text="Welcome to the LoadPAT! "
                                                        "LoadPAT is an open-source tool to anonymize load profiles "
                                                        "in different levels. "
                                                        "The possible levels of anonymization are:",
                                        font=("Arial", self.fontsize_labels), wraplength=500)
        rowcounter += 1
        general_instructions.grid(columnspan=3, column=0, row=rowcounter)

        # Level Instructions
        level_explanations = ["In Level 1 the load profile can be normalized to 1.",
                              "From Level 2 upwards features of the load profile are calculated. "
                              "In Level 2 the anonymized load profile has the same event times "
                              "and order as the original profile.",
                              "In Level 3 the anonymized load profile has the same event times "
                              "but switched event order compared to the original profile.",
                              "In Level 4 the anonymized load profile has switched resting times "
                              "but the same event order as the original profile.",
                              "In Level 5 the anonymized load profile has switched resting times"
                              "and switched event order compared to the original profile",
                              ]
        level_instructions1 = []
        level_instructions2 = []
        for i in range(5):
            level_instructions1.append(tk.Label(self.root, text="Level " + str(i+1) + ": ",
                                                font=("Arial", self.fontsize_labels)))
            level_instructions1[i].grid(column=0, row=i+2)
            level_instructions2.append(tk.Label(self.root, text=level_explanations[i],
                                                font=("Arial", self.fontsize_labels), anchor="e", justify="left",
                                                wraplength=400))
            level_instructions2[i].grid(column=1, row=i+2, sticky="W")


        # Step 1
        instructions2 = tk.Label(self.root, text="Step 1: Select a CSV file on your computer which contains "
                                                 "a load profile",
                                 font=("Arial", self.fontsize_labels), anchor="e", justify="left", wraplength=300)
        instructions2.grid(column=4, row=rowcounter, sticky="W")

        def browse_button():
            browse_text.set("Loading...")
            self.file = askopenfile(parent=self.root, mode='rb', title="Choose a file",
                                    filetypes=[("CSV file", "*.csv"), ("Excel file", "*.xlsx")])
            # self.file = open("C:/Users/Tepe/Documents/GitHub/loadpat/2021-02-18_full_day.csv", "rb") # for tests

            try:
                self.loadprofile = pd.read_csv(self.file)
            except:
                self.loadprofile = pd.read_excel(self.file.name, sheet_name="Sheet1")

            if len(self.loadprofile.columns) == 2:
                self.loadprofile = self.loadprofile.set_index(self.loadprofile.columns[0])

            # interpolation of NAN-values:
            self.loadprofile = self.loadprofile.interpolate()

            name_temp = self.file.name
            head_tail = os.path.split(name_temp)

            self.file_folder = head_tail[0]

            info_file = tk.Label(self.root, text=head_tail[1] + "\nwas succesfully loaded!",
                                 font=("Arial", self.fontsize_labels), anchor="e", justify="left", wraplength=300)
            info_file.grid(column=6, row=1, sticky="W")
            browse_text.set("Browse")

            # PLOT of original profile:
            data = dict()
            data["loadprofile"] = self.loadprofile.div(1000)
            data["ylabel"] = "Power / kW"
            data["original_or_anonym"] = "original"

            f = self.plot_function(data)
            f.suptitle("Original Load Profile", fontsize='medium')

            # Check if folder ProfilePlots exist. If not, create it.
            isExist = os.path.exists('ProfilePlots')
            if not isExist:
                os.makedirs('ProfilePlots')  # Create a new directory because it does not exist

            f.savefig('ProfilePlots\original_profile.svg', transparent=True)
            f.savefig('ProfilePlots\original_profile.pdf', transparent=True)

            canvas = FigureCanvasTkAgg(f, self.root)
            canvas.get_tk_widget().grid(rowspan=rowspan_plots, column=4, row=row_plots)

        # Browse button
        browse_text = tk.StringVar()
        browse_btn = tk.Button(self.root, textvariable=browse_text, command=lambda: browse_button(),
                               font="Arial 12 bold", bg="#ef8700",
                               fg="#707173", height=2, width=15)
        browse_text.set("Browse")
        browse_btn.grid(column=5, row=rowcounter)

        # Place Holder for information:
        info_file = tk.Label(self.root, text="                                               ",
                             font=("Arial", self.fontsize_labels), anchor="e", justify="left", wraplength=300)
        info_file.grid(column=6, row=rowcounter, sticky="W")


        # Frame for the following 3 elements
        rowcounter += 1
        temp_frame= tk.Frame(self.root)
        temp_frame.grid(columnspan=3, column=4, row=rowcounter, padx=10, pady=10)

        # Normalization Tickbox
        i = tk.IntVar()
        checkbox = tk.Checkbutton(temp_frame, text="Normalization to 1", variable=i)
        checkbox.grid(column=2, row=1)

        # Slider for Levels:
        slider = tk.StringVar()
        slider1 = tk.Scale(temp_frame, variable=slider, from_=1, to=5, length=200, orient=tk.HORIZONTAL, tickinterval=1,
                           showvalue=False, troughcolor="#ef8700")
        slider1.grid(column=2, row=2)

        # Entry box Threshold for events:
        level_instructions = tk.Label(temp_frame, text="Threshold for peak/event in % of profile average:",
                                      font=("Arial", self.fontsize_labels), anchor="e", justify="left", wraplength=300)
        level_instructions.grid(column=1, row=3, sticky="W")

        self.box_threshold = tk.Entry(temp_frame, font="Arial 12", width=10)
        self.box_threshold.insert(tk.END, threshold_default[0])
        self.box_threshold.grid(column=2, row=3)

        # Button: Update Original plot
        def update_original_plot():
            data = dict()
            data["loadprofile"] = self.loadprofile.div(1000)
            data["ylabel"] = "Power / kW"
            data["original_or_anonym"] = "original"

            f = self.plot_function(data)
            f.suptitle("Original Load Profile", fontsize='medium')
            f.savefig('ProfilePlots\original_profile.svg', transparent=True)
            f.savefig('ProfilePlots\original_profile.pdf', transparent=True)

            canvas = FigureCanvasTkAgg(f, self.root)
            canvas.get_tk_widget().grid(rowspan=rowspan_plots, column=4, row=row_plots)


            return 0

        update_text = tk.StringVar()
        update_original_plot_btn = tk.Button(temp_frame, textvariable=update_text, command=lambda: update_original_plot(),
                                  font="Arial 10 bold",
                                  bg="#ef8700",
                                  fg="#707173", height=1, width=10)
        update_text.set("Update Plot")
        update_original_plot_btn.grid(column=3, row=3)



        # Step 2
        level_instructions = tk.Label(self.root, text="Step 2: Select a Level of Anonymization",
                                      font=("Arial", self.fontsize_labels), anchor="e", justify="left", wraplength=300)
        level_instructions.grid(column=4, row=rowcounter, sticky="W")


        # Step 3
        rowcounter += 1
        instructions3 = tk.Label(self.root,
                                 text="Step 3: Click 'Anonymize' to start the anonymization of the chosen load profile",
                                 font=("Arial", self.fontsize_labels), anchor="e", justify="left", wraplength=300)
        instructions3.grid(column=4, row=rowcounter, sticky="W")

        def anonymize_file():
            anonymize_text.set("Anonymizing...")
            # Calculate profile
            loadprofile = self.loadprofile
            if self.monte_carlo_option:
                slider_value = self.level
            else:
                slider_value = slider1.get()

            self.Normalization = i.get()
            self.act_threshold= self.box_threshold.get()
            self.anonym_profile = anonymization_function(loadprofile, slider_value, self.Normalization,
                                                         self.act_threshold)
            # Save profile for SimSES
            anonym_csv_path_and_name = self.file.name[:-4] + "_anonym.csv"
            self.anonym_profile.to_csv(anonym_csv_path_and_name, header=False)

            print("File was succesfully anonymized")
            anonymize_text.set("Anonymize")

            # PLOT of anonymized profile:
            # time_original_profile = self.loadprofile.index.values.astype(int)
            # datetime_data = [datetime.fromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S') for x in time_original_profile]
            data = dict()
            if self.Normalization == 1:
                data["loadprofile"] = self.anonym_profile
                data["ylabel"] = "Normalized Power / p.u."
                titlename = "Anonymized Profile (normalized) Level " + str(slider_value)
                file_name = 'anonym_profile_level_' + str(slider_value) + ' normalized'

            else:
                data["loadprofile"] = self.anonym_profile.div(1000)
                data["ylabel"] = "Power / kW"
                titlename = "Anonymized Profile Level " + str(slider_value)
                file_name = 'anonym_profile_level_' + str(slider_value)

            data["original_or_anonym"] = "anonym"
            f = self.plot_function(data)
            f.suptitle(titlename, fontsize='medium')
            f.savefig("ProfilePlots/" + file_name + ".svg", transparent=True)
            f.savefig("ProfilePlots/" + file_name + ".pdf", transparent=True)

            canvas = FigureCanvasTkAgg(f, self.root)
            canvas.get_tk_widget().grid(rowspan=rowspan_plots, columnspan=2, column=5, row=row_plots)

            # Analyze profile:
            original, anonym = profile_comparison(loadprofile, self.anonym_profile, self.Normalization)

            self.comparison["original"] = original
            self.comparison["anonym"] = anonym

            original_infos = str()
            anonym_infos = str()

            for parameter in original:
                original_infos = original_infos + str(parameter) + ": " + str(np.round(original[parameter],
                                                                                       decimals=1)) + ", "
                anonym_infos = anonym_infos + str(parameter) + ": " + str(np.round(anonym[parameter],
                                                                                   decimals=1)) + ", "

            if isinstance(self.save_info, tk.Label):
                self.save_info.grid_forget()
            if isinstance(self.analysis_result, tk.Label):
                self.analysis_result.grid_forget()
            self.analysis_result = tk.Label(self.root,
                                            text="Original: " + original_infos + "\nAnonym: " + anonym_infos,
                                            # text="TEST",
                                            font=("Arial", self.fontsize_labels-3),
                                            anchor="e", justify="left", wraplength=180)
            self.analysis_result.grid(column=6, row=row_plots-1, sticky="W")

            return original, anonym

        # Anonymize button
        anonymize_text = tk.StringVar()
        self.level = slider1.get()
        anonymize_btn = tk.Button(self.root, textvariable=anonymize_text, command=lambda: anonymize_file(),
                                  font="Arial 12 bold",
                                  bg="#ef8700",
                                  fg="#707173", height=2, width=15)
        anonymize_text.set("Anonymize")
        anonymize_btn.grid(column=5, row=rowcounter)

        # Step 4 Analyze Profile
        # Instructions: SimSES button
        rowcounter += 4
        instructions3 = tk.Label(self.root,
                                 text="Step 4: Click 'SimSES Simulation' to run the SimSES Simulation and Analysis",
                                 font=("Arial", self.fontsize_labels),
                                 anchor="e", justify="left", wraplength=300)
        instructions3.grid(column=4, row=rowcounter, sticky="W")

        # SimSES analysis
        def simses_window(monte_carlo_option):

            if not hasattr(self, 'file'):
                text_temp = "Please start with Step 1."
            else:
                # Instance of SimSES GUI Class:
                self.file.level = self.level
                self.file.folder = self.file_folder
                simses_gui = SimSES_GUI(self.root, monte_carlo_option)
                simses_gui.create_widgets_SimSES_GUI(self.file)
                text_temp = "Simulation running."

            self.save_info = tk.Label(self.root,
                                      text=text_temp,
                                      font=("Arial", self.fontsize_labels),
                                      anchor="e", justify="left", wraplength=300)
            # self.save_info.grid(column=7, row=rowcounter, sticky="W")

            return 0

        # SimSES button

        simses_text = tk.StringVar()
        simses_btn = tk.Button(self.root, textvariable=simses_text, command=lambda: simses_window(self.monte_carlo_option), font="Arial 12 bold",
                             bg="#ef8700", fg="#707173", height=2, width=15)
        simses_text.set("SimSES Simulation")
        simses_btn.grid(column=5, row=rowcounter)

        # Instructions: Save button
        rowcounter += 1
        instructions3 = tk.Label(self.root,
                                 text="Step 5: Click 'Save' to save the anonymized profile to a specific folder",
                                 font=("Arial", self.fontsize_labels),
                                 anchor="e", justify="left", wraplength=300)
        instructions3.grid(column=4, row=rowcounter, sticky="W")

        def save_file():
            if isinstance(self.save_info, tk.Label):
                self.save_info.grid_forget()

            if not hasattr(self, 'file'):
                text_temp = "Please start with Step 1."
            elif not hasattr(self.anonym_profile, 'to_csv'):
                text_temp = "Please continue with Step 2."
            else:
                name_temp = self.file.name
                new_name = name_temp[:-4] + "_anonym.csv"

                original_filename = ntpath.basename(name_temp)
                level_of_anonymization = slider1.get()

                saving_path = tk.filedialog.asksaveasfilename(initialfile=new_name, defaultextension=".csv",
                                                              title="Save anonymized profile")
                if saving_path == '':
                    text_temp = "Not saved yet!"
                else:
                    self.anonym_profile.columns = ['Original profile name: ' + original_filename +
                                                   ', Level of Anonymization: ' + str(level_of_anonymization)]
                    self.anonym_profile.to_csv(saving_path, header=True)
                    head_tail = os.path.split(name_temp)
                    text_temp = "Anonymized profile was saved to\n" + head_tail[1][:-4] + \
                                "_anonym.csv \nin the folder: " + head_tail[0]
            self.save_info = tk.Label(self.root, text=text_temp, font="Arial", anchor="e", justify="left",
                                      wraplength=300)
            self.save_info.grid(column=7, row=rowcounter, sticky="W")

        # Save button
        save_text = tk.StringVar()
        save_btn = tk.Button(self.root, textvariable=save_text, command=lambda: save_file(), font="Arial 12 bold",
                             bg="#ef8700",
                             fg="#707173", height=2, width=15)
        save_text.set("Save")
        save_btn.grid(column=5, row=rowcounter)

        # Monte-Carlo Number:
        temp_frame_monte_carlo = tk.Frame(self.root)
        temp_frame_monte_carlo.grid(columnspan=1, column=6, row=rowcounter-1, padx=10, pady=10)

        monte_carlo_instructions = tk.Label(temp_frame_monte_carlo, text="Amount of simulations for Monte-Carlo:",
                                      font=("Arial", self.fontsize_labels), anchor="e", justify="left", wraplength=300)
        monte_carlo_instructions.grid(column=0, row=0, sticky="W")

        self.box_monte_carlo = tk.Entry(temp_frame_monte_carlo, font="Arial 12", width=10)
        self.box_monte_carlo.insert(tk.END, monte_carlo_default[0])
        self.box_monte_carlo.grid(column=0, row=1)


        # Button for Monte Carlo Simulations
        def run_several_simulations():
            monte_carlo_number = int(self.box_monte_carlo.get())

            for level in range(2,6):
                print('Monte Carlo for Level ' + str(level) + ' starts!')

                for counter_monte_carlo in range(monte_carlo_number):
                    print("Anonymization #: " + str(counter_monte_carlo))

                    self.level = level # here the level is iterated from 2 to 5

                    self.Normalization = i.get()
                    self.act_threshold = self.box_threshold.get()
                    profilename = self.file.name.rsplit('/')[-1][0:-4]
                    name_of_simulation = profilename + "_level" + str(self.level) + "_Threshold" + self.act_threshold

                    #create CSV for SimSES results:
                    if counter_monte_carlo == 0:
                        # Check if folder Results exist. If not, create it.
                        isExist = os.path.exists('Results')
                        if not isExist:
                            os.makedirs('Results')  # Create a new directory because it does not exist
                        # Check if folder Results\SimSES_Simulations exist. If not, create it.
                        isExist = os.path.exists('Results\SimSES_Simulations')
                        if not isExist:
                            os.makedirs('Results\SimSES_Simulations')  # Create a new directory because it does not exist


                        counter_folder = 1
                        while counter_folder < 100:
                            temp_name = 'Results\\SimSES_Simulations\\' + name_of_simulation  + "_" + str(counter_folder) + '.csv'
                            try:
                                f = open(temp_name)
                                counter_folder += 1
                                f.close()
                            except IOError:
                                df = pd.DataFrame(list())
                                df.to_csv(temp_name, header=False)  # Create empty CSV
                                break

                    original, anonym = anonymize_btn.invoke()
                    anonym = ast.literal_eval(anonym)

                    if counter_monte_carlo == 0:
                        self.monte_carlo_option = 1
                    else:
                        self.monte_carlo_option = 2

                    ## New variant with batch simulation
                    # 1. Save original + all anonymized load profiles
                    folder = self.file.name.rsplit('/', 1)[0]

                    # Check, if Monte Carlo folder exists:
                    if not os.path.isdir('Monte_Carlo_Profiles'):
                        os.mkdir(folder + '/Monte_Carlo_Profiles')


                    if counter_monte_carlo == 0:
                        # Create Folder for Level
                        if not os.path.isdir('Monte_Carlo_Profiles/Level' + str(self.level)):
                            os.mkdir(folder + '/Monte_Carlo_Profiles/Level' + str(self.level))
                        result_and_original_csv_path_and_name = folder + '/Monte_Carlo_Profiles/Level' + str(self.level) + \
                                                                '/' + self.file.name.rsplit('/', 1)[1]
                        self.loadprofile.to_csv(result_and_original_csv_path_and_name, header=False)

                    current_file = self.file.name.rsplit('/', 1)[1][:-4] + "_anonym" + str(counter_monte_carlo) + ".csv"
                    result_and_anonym_csv_path_and_name = folder + '/Monte_Carlo_Profiles/Level' + str(self.level) + \
                                                                '/' + current_file
                    self.anonym_profile.to_csv(result_and_anonym_csv_path_and_name, header=False)

                    if counter_monte_carlo == 0:
                        original = ast.literal_eval(original)
                        original_dataframe = pd.DataFrame(original, index=["Original"])
                        anonym_dataframe = pd.DataFrame(anonym, index=["Anonym_0"])
                        result_dataframe = pd.concat([original_dataframe, anonym_dataframe])

                    else:
                        anonym_dataframe = pd.DataFrame(anonym, index=["Anonym_" + str(counter_monte_carlo)])
                        result_dataframe = pd.concat([result_dataframe, anonym_dataframe])

                    temp_name = "Results/Comparison_" + name_of_simulation + "%.csv"
                    result_dataframe.to_csv(temp_name)

                # After all anonymized load profiles were saved: SimSES Batch - one simulation for each profile
                simses_btn.invoke()


        monte_carlo_txt = tk.StringVar()
        monte_carlo_btn = tk.Button(self.root, textvariable=monte_carlo_txt, command=lambda: run_several_simulations(), font="Arial 12 bold",
                             bg="#ef8700",
                             fg="#707173", height=2, width=15)
        monte_carlo_txt.set("Run Monte-Carlo")
        monte_carlo_btn.grid(column=6, row=rowcounter)

        self.root.mainloop()  # end elements of GUI


if __name__ == "__main__":
    main()
