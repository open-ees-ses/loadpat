# LoadPAT

LoadPAT (Load Profile Anonymization Tool) is an open-source tool that allows the easy and gradual anonymization of electric load profiles.
The tool was developed by Benedikt Tepe at the Chair of Electrical Energy Storage Technology of the Technical University of Munich within the SimBAS research project.

## **Installation**

The following packages need to be installed with version number if not the newest version is used:
- pandas
- matplotlib
- simses -> also installs several other packages


## **Execution of the anonymization**
You can either run the "LoadPAT_main.py" in the terminal or use an IDE like PyCharm to run it.
If the packages were installed correctly, a GUI opens showing 5 steps and explanations of the levels of anonymization.

1. In step 1, a loadprofile (csv or xlsx) has to be selected. If it was loaded correctly, it will be displayed beneath step 3.
2. In step 2, the level of anonymization can be selected (1 to 5). Moreover, the loadprofile can be normalized to the maximal value. Furthermore, the threshold between base sequences and peak sequences can be defined 
(as a multiple of of the profiles average). Standard is 130% of the average.
3. In step 3, the load profile is anaonymized. If the anonymization was performed succesfully, the anonymized profile will be plotted next to the original profile.
4. In step 4, SimSES (simulation of stationary energy storage systems) can be used to simulate the storage behavior once with the original and once with the anonymized profile.
More information on SimSES can be found here (https://www.epe.ed.tum.de/ees/forschungsteams/team-ses/simulation-and-optimization-toolchain/) and here (https://gitlab.lrz.de/open-ees-ses/simses).
5. In step 5, the anonymized profile can be saved to a specific folder.

<img src="doc/FigA3_Screenshot_GUI1.png">


#### SimSES battery storage simulations
Clicking the button SimSES Simulation opens a new window (SimSES Simulation and Analysis).
Here several parameters can be defined before simulating the storage behavior. The storage capacity (in kWh), the maximal storage power (in kW), the start SOC (state of charge) and the resolution of the simulation can be defined.
As lithium-ion battery type, one of three cells can be selected.
As energy management strategy a simple peak shaving strategy (SimplePeakShaving) and a self-consumption increase strategy (ResidentialPvGreedy) can be selected.
Selecting peak shaving, a peak shaving limit has to be defined.
Selecting self-consumption increase, the nominal pv power has to be defined.

Afterwards, the battery storage can be simulated once with the original and once with the anonymized profile.
Clicking "Show Results" shows the results in a table and four histograms.
Afterwards, the SimSES results can be saved. If users are satisfied, they can save the anonymized profile afterwards in the main GUI.

![Diagram](doc/FigA4_Screenshot_GUI2.png)


#### Monte-Carlo-Simulations
Next to step 5, there is a button to run X (default 100) Monte-Carlo-Simulations simultanously.
Clicking the button anonymizes the original profile X times for every level 2 to 5 and runs SimSES with each profile. 
This feature was used in the LoadPAT publication to compare the profiles and storage key performance indicators over the different levels.
To use this feature, the default parameters in the f_simses_window.py function must be adjusted, or the respective use case must be defined as "use_case_monte_carlo" (Peak-Shaving=PS, Household self-consumption increase=household_sci) in line 69. 

```
        use_case_monte_carlo = "PS" # PS, household_sci
```
