import tkinter as tk
import tkinter.ttk

from PIL import Image, ImageTk

from f_simses import do_simses
import pathlib
import os
import pandas as pd
import numpy as np
import ast
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import json
from pathlib import Path
import csv
import gzip
from os import listdir
from os.path import isfile, join

class SimSES_GUI():

    def __init__(self, root, monte_carlo_option):
        self.newWindow = tk.Toplevel(root)
        self.entry_frames = {}
        self.boxes_dic = {}
        self.simses_parameter_label = {}
        self.simses_results = {}
        self.monte_carlo_option = monte_carlo_option

    def create_widgets_SimSES_GUI(self, file):

        self.newWindow.title("SimSES Simulation and Analysis")
        self.newWindow.geometry("1200x900")  # sets the geometry of toplevel

        # 1. SimSES Logo:
        frame0 = tk.Frame(self.newWindow)
        rowcounter = 0
        frame0.grid(rowspan=2, column=0, row=rowcounter, padx=10)
        logo = Image.open('simses_logo.jpg')
        logo = logo.resize((220, 130))  ## The (250, 250) is (height, width)
        logo = ImageTk.PhotoImage(logo)
        logo_label = tk.Label(frame0, image=logo)
        logo_label.image = logo
        logo_label.grid(columnspan=2, column=0, row=0)

        # 2. Introduction Text:
        rowcounter+=2
        introduction_text = "Welcome to SimSES, a storage system simulation tool!\n\n" \
                            "SimSES allows the simulation of an energy storage based on\nstorage " \
                            "parameters and input profiles. Please choose\nthe following " \
                            "parameter to simulate a battery storage\nwith your original " \
                            "profile and your anonymized profile:"
        label = tk.Label(frame0, text=introduction_text, font="Arial 12", justify="left") # **options
        label.grid(column=0, row=rowcounter)

        # 3. Decision on Parameters:

        # Entry boxes:
        label_entries = ["Storage Capacity in kWh:", "Max. Storage Power in kW:", "Start SOC in %:", "Resolution of simulation in s:",
                         "Peak Shaving Limit in kW:", "PV Nominal Power in kW:"]
        box_entries_PS = ["140", "250", "100", "60", "32", "0"]
        box_entries_company_sci = ["600", "450", "100", "300", "100", "450"]
        box_entries_household_sci = ["8.8", "7", "100", "60", "100", "9.3"]

        ems_options = ["SimplePeakShaving", 'ResidentialPvGreedy', 'ParallelMultiUse']

        use_case_monte_carlo = "PS" # PS, company_sci, household_sci
        if use_case_monte_carlo == "PS":
            box_entries = box_entries_PS
            ems_number = 0
        elif use_case_monte_carlo == "company_sci":
            box_entries = box_entries_company_sci
            ems_number = 2
        elif use_case_monte_carlo == "household_sci":
            box_entries = box_entries_household_sci
            ems_number = 1

        for counter in range(4):
            rowcounter+=1
            self.entry_frames[counter] = tk.Frame(self.newWindow)
            self.entry_frames[counter].grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)
            self.simses_parameter_label[counter] = tk.Label(self.entry_frames[counter], text=label_entries[counter], font="Arial 12", width=23, anchor="w")
            self.simses_parameter_label[counter].grid(column=0, row=rowcounter)
            self.boxes_dic[counter] = tk.Entry(self.entry_frames[counter], font="Arial 12", width=10)
            self.boxes_dic[counter].insert(tk.END, box_entries[counter])
            self.boxes_dic[counter].grid(column=1, row=rowcounter)

        # Battery type
        rowcounter+=1
        frame_battery = tk.Frame(self.newWindow)
        frame_battery.grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)
        battery_label = tk.Label(frame_battery, text="Battery type:", font="Arial 12", width=23, anchor="w")
        battery_label.grid(column=0, row=rowcounter)

        battery_options = ["SanyoNMC", "SonyLFP", "MolicelNMC"]
        clicked_battery = tk.StringVar()
        clicked_battery.set(battery_options[0])
        drop_battery = tk.OptionMenu(frame_battery, clicked_battery, *battery_options)
        drop_battery.grid(column=1, row=rowcounter)

        # EMS
        rowcounter +=1
        frame_ems = tk.Frame(self.newWindow)
        frame_ems.grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)
        ems_label = tk.Label(frame_ems, text="Energy Management Strategy:", font="Arial 12", width=23, anchor="w")
        ems_label.grid(column=0, row=rowcounter)


        def application_specific_entries(event):
            rowcounter = 9
            counter = 4

            if len(self.entry_frames) == 5:
                self.entry_frames[counter].grid_forget()
            elif len(self.entry_frames) == 6:
                self.entry_frames[counter].grid_forget()
                self.entry_frames[counter+1].grid_forget()

            if combobox_ems.get()=="SimplePeakShaving":
                application_counter = counter
            elif combobox_ems.get()=="ResidentialPvGreedy":
                application_counter = counter+1
            elif combobox_ems.get()=="ParallelMultiUse":
                application_counter = counter

            if combobox_ems.get() == "SimplePeakShaving" or combobox_ems.get() == "ResidentialPvGreedy":
                self.entry_frames[counter] = tk.Frame(self.newWindow)
                self.entry_frames[counter].grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)
                self.simses_parameter_label[counter] = tk.Label(self.entry_frames[counter],
                                                                text=label_entries[application_counter],
                                                                font="Arial 12", width=23, anchor="w")
                self.simses_parameter_label[counter].grid(column=0, row=rowcounter)
                self.boxes_dic[counter] = tk.Entry(self.entry_frames[counter], font="Arial 12", width=10)
                self.boxes_dic[counter].insert(tk.END, box_entries[application_counter])
                self.boxes_dic[counter].grid(column=1, row=rowcounter)

            elif combobox_ems.get() == "ParallelMultiUse":
                # For Peak-Shaving Entry:
                self.entry_frames[counter] = tk.Frame(self.newWindow)
                self.entry_frames[counter].grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)
                self.simses_parameter_label[counter] = tk.Label(self.entry_frames[counter],
                                                                text=label_entries[application_counter],
                                                                font="Arial 12", width=23, anchor="w")
                self.simses_parameter_label[counter].grid(column=0, row=rowcounter)
                self.boxes_dic[counter] = tk.Entry(self.entry_frames[counter], font="Arial 12", width=10)
                self.boxes_dic[counter].insert(tk.END, box_entries[application_counter])
                self.boxes_dic[counter].grid(column=1, row=rowcounter)

                rowcounter += 1

                # For PV Entry:
                self.entry_frames[counter+1] = tk.Frame(self.newWindow)
                self.entry_frames[counter+1].grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)
                self.simses_parameter_label[counter+1] = tk.Label(self.entry_frames[counter+1],
                                                                text=label_entries[application_counter+1],
                                                                font="Arial 12", width=23, anchor="w")
                self.simses_parameter_label[counter+1].grid(column=0, row=rowcounter)
                self.boxes_dic[counter+1] = tk.Entry(self.entry_frames[counter+1], font="Arial 12", width=10)
                self.boxes_dic[counter+1].insert(tk.END, box_entries[application_counter+1])
                self.boxes_dic[counter+1].grid(column=1, row=rowcounter)


            elif combobox_ems.get() != "SimplePeakShaving" and combobox_ems.get() != "ResidentialPvGreedy" and\
                    len(self.entry_frames) == 5:
                self.entry_frames[counter].grid_forget()


        combobox_ems = tkinter.ttk.Combobox(frame_ems, value=ems_options)
        combobox_ems.current(ems_number)
        combobox_ems.grid(column=1, row=rowcounter)
        combobox_ems.bind("<<ComboboxSelected>>", application_specific_entries)

        # if SimplePeakShaving is default entry:
        if combobox_ems.get()=="SimplePeakShaving":
            application_specific_entries(event=0)
        if combobox_ems.get()=="ResidentialPvGreedy":
            application_specific_entries(event=0)
        if combobox_ems.get()=="ParallelMultiUse":
            application_specific_entries(event=0)

        # Start SimSES by clicking button
        def start_simses(orignal_or_anonym):

            simses_sign.insert(0, 'SimSES is running')
            simses_sign.configure(bg='#FF0000')
            self.newWindow.update_idletasks()

            simses_parameter = dict()
            if orignal_or_anonym=="original":
                sim_original_text.set("Simulating...")
                simses_parameter["simulation_name"] = "LoadPAT_Simulation_Original"
            elif orignal_or_anonym=="anonym":
                sim_anonym_text.set("Simulating...")
                simses_parameter["simulation_name"] = "LoadPAT_Simulation_Anonym"

            #Get values from Boxes and Dropdowns:
            simses_parameter["capacity"] = self.boxes_dic[0].get()
            simses_parameter["max_power"] = self.boxes_dic[1].get()
            simses_parameter["start_soc"] = self.boxes_dic[2].get()
            simses_parameter["resolution"] = self.boxes_dic[3].get()
            simses_parameter["ems"] = combobox_ems.get()
            if len(self.boxes_dic) >= 5:
                if combobox_ems.get() == "SimplePeakShaving":
                    simses_parameter["peak_shaving_limit"] = self.boxes_dic[4].get()
                    simses_parameter["pv_power"] = 0
                elif combobox_ems.get() == "ResidentialPvGreedy":
                    simses_parameter["peak_shaving_limit"] = 0
                    simses_parameter["pv_power"] = self.boxes_dic[4].get()
                elif combobox_ems.get() == "ParallelMultiUse":
                    simses_parameter["peak_shaving_limit"] = self.boxes_dic[4].get()
                    simses_parameter["pv_power"] = self.boxes_dic[5].get()
            else:
                simses_parameter["peak_shaving_limit"] = 0
                simses_parameter["pv_power"] = 0

            simses_parameter["battery"] = clicked_battery.get()
            do_simses(file, simses_parameter, self.monte_carlo_option)

            if orignal_or_anonym=="original":
                sim_original_text.set("Simulation of\nOriginal done")
            elif orignal_or_anonym=="anonym":
                sim_anonym_text.set("Simulation of\nanonymized done")

            simses_sign.insert(0, 'SimSES is ready')
            simses_sign.configure(bg='#32CD32')
            self.newWindow.update_idletasks()


        # SimSES original Profile and anonymized Profile Button
        rowcounter += 3
        frame_simses = tk.Frame(self.newWindow)
        frame_simses.grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)

        sim_original_text = tk.StringVar()
        original_btn = tk.Button(frame_simses, textvariable=sim_original_text, command=lambda: start_simses("original"), font="Arial 12 bold",
                             bg="#ef8700",
                             fg="#707173", height=2, width=15)
        sim_original_text.set("Simulate Original")
        original_btn.grid(column=0, row=rowcounter, padx=10)

        sim_anonym_text = tk.StringVar()
        anonym_btn = tk.Button(frame_simses, textvariable=sim_anonym_text, command=lambda: start_simses("anonym"), font="Arial 12 bold",
                             bg="#ef8700",
                             fg="#707173", height=2, width=15)
        sim_anonym_text.set("Simulate\nAnonymized")
        anonym_btn.grid(column=1, row=rowcounter, padx=10)

        # Sign, showing that SimSES is running
        rowcounter += 1
        simses_sign = tk.Listbox(frame_simses,height=1,bg='#32CD32',justify='center')
        simses_sign.grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)
        simses_sign.insert(0, 'SimSES is ready')

        def result_support_function(result_path):
            # for self-consumption-rate & self-sufficiency:
            sitelevel_eval_data= pd.read_csv(str(result_path) + "\SiteLevelEvaluation0.0.csv", header=None)
            self_consumption_index = sitelevel_eval_data[0] == "Self-consumption rate"
            self_sufficiency_index = sitelevel_eval_data[0] == "Self-sufficiency"
            try:  # if integer
                self_consumption = float(sitelevel_eval_data[1][self_consumption_index])
                self_sufficiency = float(sitelevel_eval_data[1][self_sufficiency_index])
            except ValueError:  # if string
                self_consumption = sitelevel_eval_data[1][self_consumption_index].to_string(
                    index=False)
                self_sufficiency = sitelevel_eval_data[1][self_sufficiency_index].to_string(
                    index=False)
            sitelevel_df = pd.DataFrame([["Self-consumption rate", self_consumption, "%"],
                                                  ["Self-sufficiency", self_sufficiency, "%"]])

            # for calendar and cyclic capacity loss:
            lithiumionstate_data = pd.read_csv(str(result_path) + "\LithiumIonState.csv.gz",
                                                        compression='gzip')
            cumulative_cal_loss = sum(lithiumionstate_data["Calendric Capacity Loss in Wh"]) / \
                                           lithiumionstate_data["Capacity in Wh"][0]
            cumulative_cyc_loss = sum(lithiumionstate_data["Cyclic Capacity Loss in Wh"]) / \
                                           lithiumionstate_data["Capacity in Wh"][0]
            cap_loss_df = pd.DataFrame([["Calendric capacity loss", 100 * cumulative_cal_loss, "%"],
                                                 ["Cyclic capacity loss", 100 * cumulative_cyc_loss, "%"]])

            # for c-rate:
            current = np.mean(abs(lithiumionstate_data["Current in A"]))
            capacity_ah = lithiumionstate_data["Capacity in Wh"][0] / \
                                   lithiumionstate_data["Nominal voltage in V"][0]
            c_rate_original = current / capacity_ah
            c_rate_df = pd.DataFrame([["Mean absolute C-rate", c_rate_original, "1/h"]])

            return (sitelevel_df, cap_loss_df, c_rate_df)


        def show_results(file):

            result_text = "Results:"
            label = tk.Label(self.newWindow, text=result_text, font="Arial 12 bold")  # **options
            label.grid(column=2, row=0, pady=20, padx=0, sticky = "w")

            actual_path = file.folder
            original_result_folder = str(actual_path) + "\SimSES" + "\LoadPAT_Simulation_Original"
            result_path = sorted(pathlib.Path(original_result_folder).iterdir(), key=os.path.getmtime)[-1]
            technical_eval_data_original = pd.read_csv(str(result_path) + "\LithiumIonTechnicalEvaluation1.1.csv", header=None)

            sitelevel_df_original, cap_loss_df_original, c_rate_df_anonym = result_support_function(result_path)
            # Add new values to original_dataframe
            technical_eval_data_original = pd.concat([technical_eval_data_original, c_rate_df_anonym,
                                                      sitelevel_df_original, cap_loss_df_original], axis=0)


            anonym_result_folder = str(actual_path) + "\SimSES" + "\LoadPAT_Simulation_Anonym"
            result_path2 = sorted(pathlib.Path(anonym_result_folder).iterdir(), key=os.path.getmtime)[-1]
            technical_eval_data_anonym = pd.read_csv(str(result_path2) + "\LithiumIonTechnicalEvaluation1.1.csv", header=None)

            sitelevel_df_anonym, cap_loss_df_anonym, c_rate_df_anonym = result_support_function(result_path2)
            # Add new values to anonym_dataframe
            technical_eval_data_anonym = pd.concat(
                [technical_eval_data_anonym, c_rate_df_anonym, sitelevel_df_anonym, cap_loss_df_anonym], axis=0)


            tree = tk.ttk.Treeview(self.newWindow)
            # Define columns
            tree["columns"] = ("KPI", "Original", "Anonymized", "Deviation")

            # Format our columns
            tree.column("#0", width=0, stretch=tk.NO)
            tree.column("KPI", anchor=tk.W, width=220)
            tree.column("Original", anchor=tk.W, width=120)
            tree.column("Anonymized", anchor=tk.W, width=120)
            tree.column("Deviation", anchor=tk.W, width=120)

            # Create headings
            tree.heading("#0", text="")
            tree.heading("KPI", text="Key Performance Indicator", anchor=tk.W)
            tree.heading("Original", text="Original Profile", anchor=tk.W)
            tree.heading("Anonymized", text="Anonymized Profile", anchor=tk.W)
            tree.heading("Deviation", text="Deviation", anchor=tk.W)

            technical_eval_data_anonym = technical_eval_data_anonym.rename(columns={0: 3, 1: 4, 2: 5})
            combined_dataframe = pd.concat([technical_eval_data_original, technical_eval_data_anonym], axis=1)
            combined_dataframe = combined_dataframe.reset_index(drop=True)



            # Calculate Deviation:
            deviation_list = []
            for i in range(combined_dataframe.shape[0]):
                original_value = combined_dataframe[1][i]
                anonym_value = combined_dataframe[4][i]
                # if i==1:
                #     temp_original = ast.literal_eval(original_value)
                #     temp_anonym = ast.literal_eval(anonym_value)
                #     temp = []
                #     for a_i, b_i in zip(temp_original, temp_anonym):
                #         if a_i!=0:
                #             temp2 = 100*(b_i - a_i)/a_i
                #         else:
                #             temp2 = 0
                #         temp.append(temp2)
                # else:
                try:
                    temp = 100 * (float(anonym_value) - float(original_value)) / float(original_value)
                except:
                    temp = 0
                deviation_list.append(str(np.round(temp, decimals=2)))

            deviation_dataframe = pd.DataFrame.from_dict({'6': deviation_list})
            combined_dataframe2 = pd.concat([combined_dataframe, deviation_dataframe], axis=1)

            self.simses_results = combined_dataframe2
            self.simses_results= self.simses_results.drop(columns=[3])
            self.simses_results.columns = self.simses_results.columns.astype("str")
            self.simses_results.columns = ['Category', 'Original Profile', 'Unit', 'Anonymized Profile', 'Unit', 'Deviation in %']

            # Add Data
            count=0
            for record in combined_dataframe2.values:
                tree.insert(parent='', index='end', iid=count, text='',
                            values = [str(record[0]), str(record[1]) + str(record[2]), str(record[4]) + str(record[5]), str(record[6]) + " %"])
                count +=1



            # Pack to screen
            tree.grid(column=2, row = 1)

            # Histograms of SOCs:
            lithium_ion_data_original = pd.read_csv(str(result_path) + "\LithiumIonState.csv.gz", compression='gzip')
            soc_original = 100*lithium_ion_data_original["SOC in p.u."]
            lithium_ion_data_anonym = pd.read_csv(str(result_path2) + "\LithiumIonState.csv.gz", compression='gzip')
            soc_anonym = 100*lithium_ion_data_anonym["SOC in p.u."]

            f = Figure(figsize=(6, 4.5), dpi=100)
            a = f.add_subplot(221)
            soc_hist_data = [soc_original, soc_anonym]
            a.hist(soc_hist_data, bins=20, density=False, range=(0, 100), label=['Original Profile', 'Anonymized Profile'])
            #a.legend(loc="center left", prop={'size': 7}, bbox_to_anchor=(1, 0.5))

            a.title.set_text('Histogram of SOCs')
            a.title.set_size(10)
            a.set_xlabel("SOC / %", fontsize=9)
            a.set_ylabel("Frequency", fontsize=9)

            # Histograms of C-Rates:
            current_original = lithium_ion_data_original["Current in A"]
            current_anonym = lithium_ion_data_anonym["Current in A"]
            capacity_kwh = float(self.boxes_dic[0].get())

            with open(str(result_path) + "\SystemParameters.txt") as temp:
                contents = temp.read()
            system_param_str = contents[contents.find('parameters') + 13:]
            system_param_str = str(system_param_str).replace("'", '"')
            system_param_dict = json.loads(system_param_str)
            nominal_voltage = float(system_param_dict["subsystems"][0]["subsystems"][0]["nominal_voltage"])
            capacity_in_Ah = capacity_kwh*1000/nominal_voltage

            c_rates_original = current_original/capacity_in_Ah
            c_rates_anonym = current_anonym/capacity_in_Ah
            c_rates_hist_data = [c_rates_original, c_rates_anonym]

            b = f.add_subplot(222)
            b.hist(c_rates_hist_data, bins=20, density=False, range=(-1, 1), label=['Original Profile', 'Anonymized Profile'])
            b.title.set_text('Histogram of C-rates')
            b.title.set_size(10)
            b.set_xlabel("C-Rate / 1/h", fontsize=9)
            b.set_ylabel("Frequency", fontsize=9)

            # Histogram of DOD:
            # Using SOC, DODs are calculated:
            soc_original_diff = np.diff(soc_original)
            diff_change_of_sign = (np.diff(np.sign(soc_original_diff)) != 0)*1
            idx_changes = np.where(diff_change_of_sign==1)[0]
            idx_changes = np.insert(idx_changes, 0, -1) # add 0 as starting index
            doc_original = []
            for counter in range(len(idx_changes) - 1):
                start_id = idx_changes[counter]+1
                end_id = idx_changes[counter + 1]
                doc_original.append(np.sum(soc_original_diff[start_id:end_id+1]))
            dod_original = [-i for i in doc_original if i < 0]

            soc_anonym_diff = np.diff(soc_anonym)
            diff_change_of_sign_anonym = (np.diff(np.sign(soc_anonym_diff)) != 0)*1
            idx_changes_anonym = np.where(diff_change_of_sign_anonym==1)[0]
            idx_changes_anonym = np.insert(idx_changes_anonym, 0, -1) # add 0 as starting index
            doc_anonym = []
            for counter in range(len(idx_changes_anonym) - 1):
                start_id = idx_changes_anonym[counter]+1
                end_id = idx_changes_anonym[counter + 1]
                doc_anonym.append(np.sum(soc_original_diff[start_id:end_id+1]))
            dod_anonym = [-i for i in doc_anonym if i < 0]

            dod_hist_data = [dod_original, dod_anonym]

            c = f.add_subplot(223)
            c.hist(dod_hist_data, bins=20, density=False, range=(0, 100),
                   label=['Original Profile', 'Anonymized Profile'])
            c.title.set_text('Histogram of DODs')
            c.title.set_size(10)
            c.set_xlabel("DOD / %", fontsize=9)
            c.set_ylabel("Frequency", fontsize=9)

            # Histogram of temperature:
            temperatures_original = lithium_ion_data_original["Temperature in K"]
            temperatures_anonym= lithium_ion_data_original["Temperature in K"]
            temperatures_hist_data = [temperatures_original, temperatures_anonym]

            d = f.add_subplot(224)
            d.hist(temperatures_hist_data, bins=20, density=False, range=(290, 310),
                   label=['Original', 'Anonymized'])
            d.title.set_text('Histogram of Temperatures')
            d.title.set_size(10)
            d.set_xlabel("Temperature / K", fontsize=9)
            d.set_ylabel("Frequency", fontsize=9)

            f.tight_layout(rect=[0, 0, 1, 1])
            f.axes[0].grid(True)
            f.axes[1].grid(True)
            f.axes[2].grid(True)
            f.axes[3].grid(True)

            handles, labels = d.get_legend_handles_labels()
            f.legend(handles, labels, loc=(0.36, 0.5), prop={'size': 8}, ncol=2)

            canvas = FigureCanvasTkAgg(f, self.newWindow)
            canvas.get_tk_widget().grid(rowspan=12, columnspan=1, column=2, row=4)


        # Results Button
        rowcounter +=1
        frame_results = tk.Frame(self.newWindow)
        frame_results.grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)

        show_results_text = tk.StringVar()
        show_results_btn = tk.Button(frame_results, textvariable=show_results_text, command=lambda: show_results(file), font="Arial 12 bold",
                             bg="#ef8700",
                             fg="#707173", height=2, width=15)
        show_results_text.set("Show Results")
        show_results_btn.grid(columnspan=2, column=0, row=rowcounter)


        # Save SimSES results function:
        def save_simses_results(file):

            if self.monte_carlo_option == 0:
                SAVING_PATH = tk.filedialog.asksaveasfilename(initialfile='SimSES_Results', defaultextension=".csv",
                                                          title="Save SimSES results")

                if SAVING_PATH == '':
                    text_temp = "Not saved yet!"
                else:
                    self.simses_results.to_csv(SAVING_PATH, header=True)
                    text_temp = "Results saved!"

                save_results_text.set(text_temp)

            else:

                # For original + anonym:
                filefolder = os.getcwd() + "/Monte_Carlo_Profiles/Level" + str(file.level)
                allfile_list = [f for f in listdir(filefolder) if isfile(join(filefolder, f))]
                if os.path.isfile(filefolder + '/SBAP_PV_EEN_Power_Munich_2014.csv.gz'):
                    allfile_list.remove('SBAP_PV_EEN_Power_Munich_2014.csv.gz')  # Get rid of PV profile

                actual_path = pathlib.Path().resolve()

                counter = 0
                for filename in allfile_list:
                    result_folder = str(actual_path) + "/Results/" + filename + "/"
                    result_path = sorted(pathlib.Path(result_folder).iterdir(), key=os.path.getmtime)[-1]
                    technical_eval_data = pd.read_csv(
                        str(result_path) + "\LithiumIonTechnicalEvaluation1.1.csv",
                        header=None)
                    sitelevel_df, cap_loss_df, c_rate_df = result_support_function(result_path)
                    # Add new values to original_dataframe
                    technical_eval_data_total = pd.concat([technical_eval_data, c_rate_df,
                                                              sitelevel_df, cap_loss_df], axis=0)
                    if counter == 0:
                        all_data = technical_eval_data_total
                        data = []
                        data.insert(0, {0: 'Category', 1: 'Original_Profile', 2: ''})
                        all_data = pd.concat([pd.DataFrame(data), all_data], ignore_index=True)
                        counter+=1

                    else:
                        data = []
                        data.insert(0, {0: 'Category', 1: 'Anonymized_Profile_' + str(counter), 2: ''})
                        technical_eval_data_total = pd.concat([pd.DataFrame(data), technical_eval_data_total], ignore_index=True)
                        all_data = pd.concat([all_data, technical_eval_data_total[1]], axis=1)
                        counter +=1

                all_data = all_data.reset_index(drop=True)
                all_data_T = all_data.T
                all_data_T.columns = all_data_T.iloc[0]
                all_data_T = all_data_T[1:]

                newest_csv = sorted(Path("Results\\SimSES_Simulations\\").iterdir(), key=os.path.getmtime)[-1].name
                all_data_T.to_csv("Results\\SimSES_Simulations\\" + newest_csv, mode='a', index=False, header=True)

                print('Batch Results saved in ' + newest_csv + '!')


        # Save SimSES Results Button
        rowcounter +=1
        frame_results = tk.Frame(self.newWindow)
        frame_results.grid(columnspan=2, column=0, row=rowcounter, padx=10, pady=10)

        save_results_text = tk.StringVar()
        save_results_btn = tk.Button(frame_results, textvariable=save_results_text, command=lambda: save_simses_results(file), font="Arial 12 bold",
                             bg="#ef8700",
                             fg="#707173", height=2, width=20)
        save_results_text.set("Save SimSES Results")
        save_results_btn.grid(columnspan=2, column=0, row=rowcounter)

        if self.monte_carlo_option:
            # if self.monte_carlo_option == 1:
            #    original_btn.invoke()
            anonym_btn.invoke()
            # show_results_btn.invoke()
            save_results_btn.invoke()
            self.newWindow.destroy()
        return 0