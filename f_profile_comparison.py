import numpy as np
import datetime

def profile_comparison(loadprofile, anonymized_profile, normalization):

    if type(loadprofile.index[1]) == int or type(loadprofile.index[1]) == np.int64:
        time_resolution = loadprofile.index[1] - loadprofile.index[0] # seconds
    elif type(loadprofile.index[1]) == str:

        time_formats = ['%d.%m.%Y %H:%M:%S', '%d.%m.%Y %H:%M', '%d.%m.%y %H:%M:%S', '%m.%d.%y %H:%M',
                        '%m.%d.%y %H:%M:%S', '%d/%m/%Y %H:%M:%S', '%d/%m/%Y %H:%M']
        for test_format in time_formats:
            try:
                timedelta = datetime.datetime.strptime(loadprofile.index[2], test_format) \
                            - datetime.datetime.strptime(loadprofile.index[1], test_format)
                time_resolution = timedelta.total_seconds()
                break
            except ValueError:
                if test_format == time_formats[-1]:
                    time_resolution = 1
                    print("Timeformat does not exist yet! The following formats are possible: " +
                          '[%s]' % ', '.join(map(str, time_formats)))
                    pass

    loadprofile_values = loadprofile.values / 1000

    if normalization:
        anonymized_profile_values = anonymized_profile.values
    else:
        anonymized_profile_values = anonymized_profile.values / 1000

    # Original Profile
    original = dict()
    original["mean"] = np.mean(loadprofile_values)
    original["std"] = np.std(loadprofile_values)
    original["max"] = np.max(loadprofile_values)
    original["energy"] = np.sum(loadprofile_values) * time_resolution/3600 # as original is kW => kWh

    anonym = dict()
    anonym["mean"] = np.mean(anonymized_profile_values)
    anonym["std"] = np.std(anonymized_profile_values)
    anonym["max"] = np.max(anonymized_profile_values)
    anonym["energy"] = np.sum(anonymized_profile_values) * time_resolution/3600 # anonymized profile is in kW => kWh

    return original, anonym