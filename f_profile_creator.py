import random

import numpy as np

def sequence_creation(features, event_or_resting, number):

    event_variant = "successively_with_diff" # mean_diff_and_smooth, successively_with_diff
    resting_variant = "successively_with_diff" # mean_diff_and_smooth, successively_with_diff

    if event_or_resting == "event":
        temp_mean = features["mean event no ramp"][number]
        # temp_std = features["std event no ramp"][number]
        temp_length_no_ramp = features["length event no ramp"][number]
        temp_max = features["max of event"][number]
        temp_min = features["min of event"][number]
        temp_delta_mean = features["mean diff event"][number]
        temp_delta_std = features["std diff event"][number]
        temp_prob_change_sign = features["prob change sign event"][number]

    else:
        temp_mean = features["mean resting"][number]
        # temp_std = features["std resting"][number]
        temp_length_no_ramp = features["length resting"][number]
        temp_max = features["max of resting"][number]
        temp_min = features["min of resting"][number]
        temp_delta_mean = features["mean diff resting"][number]
        temp_delta_std = features["std diff resting"][number]
        temp_prob_change_sign = features["prob change sign resting"][number]

    # Variant 1: Mean Diff and Smooth
    if (event_or_resting == "event" and event_variant == "mean_diff_and_smooth") or \
        (event_or_resting == "resting" and resting_variant == "mean_diff_and_smooth"):

        test=1
        # random_values = np.abs(np.random.randn(temp_length_no_ramp))
        # # Return a sample (or samples) from the “standard normal” distribution.
        # # Sign change probability used
        # list_new = [1]
        # for counter_random in range(len(random_values) - 1):
        #     plus_or_minus = \
        #         random.choices([-1, 1], weights=(temp_prob_change_sign, 1 - temp_prob_change_sign),
        #                        k=1)[0]
        #     # 66% to change sign
        #     list_new.append(list_new[counter_random] * plus_or_minus)
        #
        # # Test: Erstellung einer neuen Reihe aus den Diff-Werten mit gewünschtem Vorzeichen:
        # diff_random_values = np.abs(np.diff(np.random.randn(temp_length_no_ramp)))
        # diff_random_values.append(0)
        # test = diff_random_values * list_new
        #
        # test2 = np.cumsum(test)
        # test_diff_binary = np.diff(np.sign(np.diff(test2)))
        # test_analysis = np.count_nonzero(np.abs(test_diff_binary) == 2) / len(test_diff_binary)
        #
        #
        # ## End
        #
        # random_sign_and_std_deviation = list_new * random_values
        #
        # diff_new_list = np.diff(list_new)
        # random_and_std_list = np.diff(np.sign(np.diff(random_sign_and_std_deviation)))
        #
        # temp_profile = temp_std * \
        #                 random_sign_and_std_deviation + \
        #                 temp_mean
        #
        # temp_profile_diff = np.diff(np.sign(np.diff(temp_profile)))
        # try:
        #     list_new_analysis = np.count_nonzero(np.abs(diff_new_list) == 2) / len(diff_new_list)
        #     random_and_std_analysis = np.count_nonzero(np.abs(random_and_std_list) == 2) / len(random_and_std_list)
        #
        #     temp_profile_diff_analysis = np.count_nonzero(np.abs(temp_profile_diff) == 2) / len(temp_profile_diff)
        #     print("Original: " + str(temp_prob_change_sign) + " VZ-Wechsel. Bei Länge " + str(len(temp_profile)))
        #     print("Anonym: " + str(temp_profile_diff_analysis) + " VZ-Wechsel")
        # except ZeroDivisionError:
        #     print("Länge Diff einmal 0")
        #
        #
        # temp_profile[temp_profile < 0] = 0
        #
        # if temp_length_no_ramp > 1:
        #     # smoothing depends on length of event profile
        #     diff_list = list()
        #     for window_size in range(10):
        #         test1 = pd.Series(temp_profile).rolling(window_size)
        #         temp_profile_smooth = test1.mean().tolist()
        #         temp_profile_smooth[0:window_size - 1] = temp_profile[0:window_size - 1]
        #         current_diff = np.mean(abs(np.diff(temp_profile_smooth)))
        #         diff_list.append(current_diff)
        #
        #     window_size = int(np.nanargmin(abs(np.array(diff_list) - temp_delta_mean)))
        #     profile_smooth = pd.Series(temp_profile).rolling(window_size)
        #     profile_smooth = profile_smooth.mean().tolist()
        #     if window_size != 1:
        #         mean_values = np.mean(temp_profile[0:window_size - 1])
        #         profile_smooth[0:window_size - 1] = [mean_values] * (
        #                 window_size - 1)  # values until window_size are NAN
        #     else:
        #         mean_values = temp_profile[0]
        #         profile_smooth[0] = mean_values
        # else:
        #     profile_smooth = temp_profile
        #
        # return profile_smooth

    # Variant 2: Successively create load profile with mean and std of delta of values of original
    elif (event_or_resting == "event" and event_variant == "successively_with_diff") or \
            (event_or_resting == "resting" and resting_variant == "successively_with_diff"):

        y_values = list()
        plus_or_minus_list = list()
        if temp_length_no_ramp > 0:  # only if length of event >0
            y_values.append(temp_mean)

            temp_sign = 1
            for k in range(1, temp_length_no_ramp):
                plus_or_minus = random.choices([-1, 1], weights=(temp_prob_change_sign,
                                                                 1 - temp_prob_change_sign), k=1)[0]
                temp_sign = plus_or_minus * temp_sign
                plus_or_minus_list.append(temp_sign)

                new_value = y_values[k - 1] + temp_sign * (temp_delta_std * np.abs(random.random()) + temp_delta_mean)

                if new_value > temp_max:
                    temp = y_values[k - 1] - temp_sign * (
                                temp_delta_std * np.abs(random.random()) + temp_delta_mean)
                    if temp < temp_min:
                        new_value = temp_max
                    else:
                        new_value = temp
                elif new_value < temp_min:
                    temp = y_values[k - 1] - temp_sign * (
                                temp_delta_std * np.abs(random.random()) + temp_delta_mean)
                    if temp > temp_max:
                        new_value = temp_min
                    else:
                        new_value = temp
                    # new_value = temp_max
                # elif new_value < temp_min:
                #     new_value = temp_min

                # if new_value > temp_max or new_value < temp_min:
                #     new_value = y_values[k - 1] - temp_sign * (
                #                 temp_delta_std * np.abs(random.random()) + temp_delta_mean)
                if new_value < 0:
                    new_value = 0

                y_values.append(new_value)
            # try:
            #     test = np.count_nonzero(np.abs(test) == 2) / len(test)
            #     test2_2 = np.count_nonzero(np.abs(test2) == 2) / len(test2)
            #     print("Länge Sequenz: " + str(len(test2)))
            #     print("VZ-Wechsel Wahrscheinlichkeit Original: " + str(temp_prob_change_sign))
            #     print("VZ-Wechsel Anonym:" + str(test2_2))
            # except ZeroDivisionError:
            #     print("Länge Diff einmal 0")

        # Scale to mean-value:
        # mean_y_values = np.mean(y_values)
        # if mean_y_values > 0:
        #     y_values = [i * temp_mean/mean_y_values for i in y_values]

        return y_values


def profile_creator(features, slider_value):

    similar_resting = 1
    similar_events = 1

    if slider_value == 3:
        similar_events = 0
    elif slider_value == 4:
        similar_resting = 0
    elif slider_value == 5:
        similar_resting = 0
        similar_events = 0

    if similar_resting:
        resting_order = np.array(range(features["number resting"]))
    else:
        resting_order = np.random.permutation(features["number resting"])

    if similar_events:
        event_order = np.array(range(features["number events"]))
    else:
        event_order = np.random.permutation(features["number events"])

    syn_load = np.empty(0)

    counter_resting = 0
    for counter_events in range(features["number events"]):

        if features['start and end sequence'][0] == 'Resting' or counter_events > 0:
            # Resting before event:
            resting_number = resting_order[counter_resting]
            if features["start and end sequence"][1] == 'Resting' \
                or counter_events < features["number events"]-1: # End is resting or not last event
                resting_number_after_event = resting_order[counter_resting+1]
            else:
                resting_number_after_event = np.nan # if profile ends with event sequence

            event_or_resting = "resting"
            resting_profile_smooth = sequence_creation(features, event_or_resting, resting_number)
            mean_y_values = np.mean(resting_profile_smooth)
            mean_original = features["mean resting"][resting_number]
            y_values = []
            if mean_y_values > 0:
                y_values = [i * mean_original / mean_y_values for i in resting_profile_smooth]
            else:
                y_values = resting_profile_smooth
            syn_load = np.append(syn_load, np.transpose(y_values))
            counter_resting += 1
        else:
            resting_number_after_event = resting_order[counter_resting]

        # Event
        event_number = event_order[counter_events]

        # Ramp up
        # Variant 1: With last value of previous sequence
        syn_event = []
        ramp_up_length = features["ramp up length"][event_number]
        if ramp_up_length > 0:
            if np.any(syn_load):
                previous_value = syn_load[-1]
            else:
                previous_value = np.mean(features["mean resting"])

            slope_up = (features["mean event no ramp"][event_number] - previous_value) / ramp_up_length
            syn_load_ramp_up = previous_value + np.cumsum(np.tile(slope_up,ramp_up_length))
            syn_event = np.append(syn_event, syn_load_ramp_up)

        # Main event
        event_or_resting = "event"
        y_diff = sequence_creation(features, event_or_resting, event_number)
        syn_event = np.append(syn_event, np.transpose(y_diff))

        # Ramp down
        # Variant 1: take last value of event and ramp down length to calculate ramp down slope
        ramp_down_length = features["ramp down length"][event_number]
        if ramp_down_length > 0:
            if not np.isnan(resting_number_after_event):
                slope_down = (features["mean resting"][resting_number_after_event] - syn_event[-1])/ramp_down_length
                syn_load_ramp_down = syn_event[-1] + np.cumsum(np.tile(slope_down,ramp_down_length))
                syn_event = np.append(syn_event, syn_load_ramp_down)
            else:
                slope_down = (np.mean(features["mean resting"]) - syn_event[-1]) / ramp_down_length
                syn_load_ramp_down = syn_event[-1] + np.cumsum(np.tile(slope_down, ramp_down_length))
                syn_event = np.append(syn_event, syn_load_ramp_down)

        # Scale to original mean:
        mean_y_values = np.mean(syn_event)
        mean_original = features["mean event"][event_number]
        y_values = []
        if mean_y_values > 0:
            y_values = [i * mean_original / mean_y_values for i in syn_event]
        else:
            y_values = syn_event
        syn_load = np.append(syn_load, np.transpose(y_values))


    # Resting after last event:

    if resting_order.any() and not np.isnan(resting_number_after_event):
        resting_number = resting_order[-1]
        event_or_resting = "resting"
        resting_profile_smooth = sequence_creation(features, event_or_resting, resting_number)
        mean_y_values = np.mean(resting_profile_smooth)
        mean_original = features["mean resting"][resting_number]
        y_values = []
        if mean_y_values > 0:
            y_values = [i * mean_original / mean_y_values for i in resting_profile_smooth]
        else:
            y_values = resting_profile_smooth
        if 'syn_load' in locals():
            syn_load = np.append(syn_load, np.transpose(y_values))
    return syn_load
