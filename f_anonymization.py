import pandas as pd
import numpy as np

from f_profile_analyser import profile_analyser
from f_profile_creator import profile_creator

def anonymization_function(load_profile, slider_value, Normalization, threshold_event):

    #slider_value = 4
    if slider_value==1:
        if Normalization:
            new_profile = load_profile/load_profile.values.max()
        else:
            new_profile = load_profile

    if slider_value>1:
        # Calculate features from original profile
        features = profile_analyser(load_profile, threshold_event)

        mean_original = np.mean(load_profile.values)
        # sum_resting = sum([a*b for a,b in zip(features["mean resting"],features["length resting"])])
        # sum_event = sum([a*b for a,b in zip(features["mean event no ramp"],features["length event no ramp"])])
        # len_resting = sum(features["length resting"])
        # len_event = sum(features["length event no ramp"])
        # mean_features = sum(sum_resting, sum_event)/sum(len_resting, len_event)


        if features["number events"] == 0 or features["number resting"]==0:
            raise ValueError('Please specify a threshold between base and peak sequences in a range resulting in '
                             'at least one resting and one peak sequence!')

        # Create anonymized profile
        anonym_profile = profile_creator(features, slider_value)

        if Normalization:
            anonym_profile = anonym_profile/np.max(anonym_profile)

        # Make Dataframe
        new_profile = pd.DataFrame(anonym_profile)
        new_profile.index = load_profile.index.copy()

        #anonym_profile = load_profile/load_profile.values.max()

    return new_profile