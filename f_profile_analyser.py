import numpy as np


def profile_analyser(load, threshold_event):
    # There are two types of profiles:
    # event-based profiles: almost zero or load event
    # classic profiles: almost never zero, base load and peaks

    # Thresholds and factors:
    # resting_threshold = 0.02 * load.values.mean()
    factor_ramping_up = 1
    factor_ramping_down = 1

    features = dict()

    # Cleaning
    load = load.values
    load[load < 0] = 0

    # Event-based or classic load profile:
    # if sum(load.values>resting_threshold)/len(load)> 0.98:
    #     event_profile = 0 # if 1: event-based, if 0: classic
    # else:
    #     event_profile = 1

    # For both types of profiles, the difference between peak/event and base/resting is calculated the same!
    # Search for events:

    threshold_events = load.mean()*int(threshold_event)/100
    idx_events = (load > threshold_events).transpose()

    idx_events = np.multiply(idx_events, 1)
    change_idx_events = (np.diff(idx_events))

    event_starts = np.array(np.where(change_idx_events == 1)[1]+1)
    if idx_events[0][0] == 1: # if profile starts with event
        event_starts = np.insert(event_starts, 0,0)

    event_ends = np.array(np.where(change_idx_events == -1)[1]+1)
    if idx_events[0][-1] == 1: # if profile ends with event
        event_ends = np.append(event_ends,len(idx_events[0]))


    # The areas contain one value before and one value after the threshold is surpassed

    # Resting characterisation
    resting_starts = event_ends + 1
    resting_ends = event_starts - 1
    resting_starts = resting_starts[(resting_starts > 0) & (resting_starts <= len(idx_events[0]))]
    resting_ends = resting_ends[(resting_ends > 0) & (resting_ends <= len(idx_events[0]))]

    # if resting is only 1 timestep, the prior and after events try to use the value as part of the event.
    # This then counts as one event and not two events
    similar_in_resting_and_event = np.intersect1d(resting_starts,event_starts).tolist()
    for similar_values in similar_in_resting_and_event:
        event_starts = np.delete(event_starts, np.argwhere(event_starts == similar_values).astype(int))
        event_ends = np.delete(event_ends, np.argwhere(event_ends == similar_values-1).astype(int))
        resting_starts = np.delete(resting_starts, np.argwhere(resting_starts == similar_values).astype(int))
        resting_ends = np.delete(resting_ends, np.argwhere(resting_ends == similar_values-1).astype(int))

    similar_in_event_and_event = np.intersect1d(event_ends,event_starts).tolist()
    for similar_values in similar_in_event_and_event:
        event_starts = np.delete(event_starts, np.argwhere(event_starts == similar_values).astype(int))
        event_ends = np.delete(event_ends, np.argwhere(event_ends == similar_values).astype(int))
        resting_starts = np.delete(resting_starts, np.argwhere(resting_starts == similar_values+1).astype(int))
        resting_ends = np.delete(resting_ends, np.argwhere(resting_ends == similar_values-1).astype(int))

    similar_in_resting_and_resting = np.intersect1d(resting_starts,resting_ends).tolist()
    for similar_values in similar_in_resting_and_resting:
        event_starts = np.delete(event_starts, np.argwhere(event_starts == similar_values+1).astype(int))
        event_ends = np.delete(event_ends, np.argwhere(event_ends == similar_values-1).astype(int))
        resting_starts = np.delete(resting_starts, np.argwhere(resting_starts == similar_values).astype(int))
        resting_ends = np.delete(resting_ends, np.argwhere(resting_ends == similar_values).astype(int))

    # # What if Event starts before another event has ended (because of the 1 value before and after event as part of event)?
    # for event_end_index in range(len(event_ends)-1):
    #     if event_ends[event_end_index] > event_starts[event_end_index+1]:
    #         print("Problem")
    #         event_starts = np.delete(event_starts, event_end_index+1)
    #         event_ends = np.delete(event_ends, event_end_index)
    #         resting_starts = np.delete(resting_starts, event_end_index+1)
    #         resting_ends = np.delete(resting_ends, event_end_index+1)

    if event_starts.any():
        if event_starts[0] > 0: # Start with resting sequence
            resting_starts = np.insert(resting_starts,0,0)

        if event_ends[-1] < len(idx_events[0]): # End with a resting sequence
            resting_ends = np.append(resting_ends, len(load))

    # Number of events
    features["number events"] = len(event_starts)
    # Number of resting sequences
    features["number resting"] = len(resting_starts)

    # Different types of profiles:
    # 1. Start with Resting, end with Resting => features["number events"] < features["number resting"]
    # 2. Start with Event, end with Resting => event_starts[0] == 0
    # 3. Start with Resting, end with Event => event_ends[-1] == len(load)
    # 4. Start with Event, end with Event => features["number events"] > features["number resting"]
    if features["number events"] < features["number resting"]:
        features["start and end sequence"] = ["Resting", "Resting"]
    elif features["number events"] > features["number resting"]:
        features["start and end sequence"] = ["Event", "Event"]
    elif event_starts[0] == 0:
        features["start and end sequence"] = ["Event", "Resting"]
    elif event_ends[-1] == len(load):
        features["start and end sequence"] = ["Resting", "Event"]





    # Length of events (without last value of resting before and first value of resting after event)
    #length_events = event_ends - event_starts

    # Event specific features
    len_events = list()
    len_events_no_ramp = list()
    max_of_events = list()
    min_of_events = list()
    mean_diff_event = list()
    std_diff_event = list()
    mean_event_no_ramp = list()
    mean_event = list()
    std_event_no_ramp = list()
    ramp_up_len = list()
    ramp_up_event = list()
    ramp_down_len = list()
    ramp_down_event = list()

    len_resting = list()
    mean_resting = list()
    std_resting = list()
    max_resting = list()
    min_resting = list()
    mean_diff_resting = list()
    std_diff_resting = list()
    prob_change_of_sign_event = list()
    prob_change_of_sign_resting = list()

    for event_counter in range(features['number events']):

        # Actual Event
        temp_event = load[event_starts[event_counter]:event_ends[event_counter]+1]
        len_events.append(len(temp_event))

        temp_mean_total_event = np.mean(temp_event)

        # Ramp up
        if event_counter==0 and features["start and end sequence"][0] == 'Event': # if profile starts with event => no ramp up
            end_ramp_up = 0
            temp_ramp_up_event = 0
            temp_ramp_up_len = 0
        else:
            end_ramp_up = np.where(temp_event >= factor_ramping_up * temp_mean_total_event)[0][0]
            temp_ramp_up_len = end_ramp_up
            if temp_ramp_up_len != 0:
                temp_ramp_up_event = (temp_event[end_ramp_up] - temp_event[0]) / temp_ramp_up_len
            else:
                temp_ramp_up_event = 0

        # Ramp down
        # Only if not last event and profile ends with event!
        if event_counter == features['number events']-1 and features["start and end sequence"][1] == 'Event':
            start_ramp_down = len(load)
            temp_ramp_down_event = 0
            temp_ramp_down_len = 0
        else:
            start_ramp_down = np.where(temp_event >= factor_ramping_down * temp_mean_total_event)[0][-1]
            if start_ramp_down == len(temp_event) or start_ramp_down == len(temp_event)-1:
                start_ramp_down -= 1
            temp_ramp_down_len = len(temp_event) - start_ramp_down -1
            temp_ramp_down_event = (temp_event[-1] - temp_event[start_ramp_down]) / temp_ramp_down_len

        load_event_no_ramp = temp_event[end_ramp_up:start_ramp_down+1]
        len_events_no_ramp.append(len(load_event_no_ramp))
        # If an event is just one peak, the event_no_ramp length can be 0.
        # Here we juts have ramp up for one time step and ramp down for one timestep

        # Maximum of Event
        if len(load_event_no_ramp) > 0:
            max_of_events.append(np.max(load_event_no_ramp))
            min_of_events.append(np.min(load_event_no_ramp))
        else: # if length of event_no_ramp==0
            max_of_events.append(np.max(temp_event[end_ramp_up]))
            min_of_events.append(np.min(temp_event[end_ramp_up]))

        # Difference from one value to the next one
        diff_between_values = np.diff(load_event_no_ramp, axis=0)
        if len(load_event_no_ramp) > 1:
            mean_diff_event.append(np.mean(abs(diff_between_values)))
            std_diff_event.append(np.std(diff_between_values))
        else: # if length of event_no_ramp==0
            mean_diff_event.append(0)
            std_diff_event.append(0)

        temp_change_sign = np.diff(np.sign(diff_between_values), axis=0)
        if len(temp_change_sign) != 0:
            prob_change_of_sign_event.append(np.count_nonzero(temp_change_sign)/len(temp_change_sign))
        else:
            # if temp_resting only 2 values: diff_between_values_resting has only one value, temp_change_sign is empty
            prob_change_of_sign_event.append(0)


        # Save in lists
        if len(load_event_no_ramp) > 0:
            mean_event_no_ramp.append(np.mean(load_event_no_ramp))
            std_event_no_ramp.append(np.std(load_event_no_ramp))
        else: # if length of event_no_ramp==0
            mean_event_no_ramp.append(np.mean(temp_event[end_ramp_up]))
            std_event_no_ramp.append(np.std(temp_event[end_ramp_up]))

        mean_event.append(temp_mean_total_event)
        ramp_up_len.append(temp_ramp_up_len)
        ramp_up_event.append(temp_ramp_up_event)
        ramp_down_len.append(temp_ramp_down_len)
        ramp_down_event.append(temp_ramp_down_event)

    # Resting periods

    for resting_counter in range(features['number resting']):
        temp_resting = load[resting_starts[resting_counter]:resting_ends[resting_counter]+1]
        len_resting.append(len(temp_resting))
        mean_resting.append(np.mean(temp_resting))
        std_resting.append(np.std(temp_resting))

        # Maximum of Resting
        if len(temp_resting) > 0:
            max_resting.append(np.max(temp_resting))
            min_resting.append(np.min(temp_resting))
        else:
            max_resting.append(0)
            min_resting.append(0)

        # Difference from one value to the next one
        diff_between_values_resting = np.diff(temp_resting, axis=0)
        mean_diff_resting.append(np.mean(abs(diff_between_values_resting)))
        std_diff_resting.append(np.std(diff_between_values_resting))

        temp_change_sign = np.diff(np.sign(diff_between_values_resting), axis=0)
        if len(temp_change_sign) != 0:
            prob_change_of_sign_resting.append(np.count_nonzero(temp_change_sign)/len(temp_change_sign))
        else:
            # if temp_resting only 2 values: diff_between_values_resting has only one value, temp_change_sign is empty
            prob_change_of_sign_resting.append(0)


    # Save all features
    features["length event"] = len_events
    features["length resting"] = len_resting
    features["mean resting"] = mean_resting
    features["mean diff resting"] = mean_diff_resting
    features["std diff resting"] = std_diff_resting
    features["max of resting"] = max_resting
    features["min of resting"] = min_resting
    features["prob change sign resting"] = prob_change_of_sign_resting

    features["length event no ramp"] = len_events_no_ramp
    features["max of event"] = max_of_events
    features["min of event"] = min_of_events
    features["mean diff event"] = mean_diff_event
    features["std diff event"] = std_diff_event
    features["prob change sign event"] = prob_change_of_sign_event

    features["mean event"] = mean_event
    features["mean event no ramp"] = mean_event_no_ramp
    features["ramp up length"] = ramp_up_len
    features["ramp down length"] = ramp_down_len

    # features["std resting"] = std_resting
    # features["std event no ramp"] = std_event_no_ramp
    # features["ramp up event"] = ramp_up_event
    # features["ramp down event"] = ramp_down_event

    return features