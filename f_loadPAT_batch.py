from configparser import ConfigParser
from simses.commons.config.generation.simulation import SimulationConfigGenerator
from simses.commons.config.generation.analysis import AnalysisConfigGenerator
from simses.simulation.batch_processing import BatchProcessing

import os
from os import listdir
from os.path import isfile, join
import pandas as pd

class LoadPATBatchProcessing(BatchProcessing):
    """
    This is the LoadPAT BatchProcessing.
    """

    def __init__(self, file, simses_parameter):
        super().__init__(do_simulation=True, do_analysis=True)
        self.__file = file
        self.__simses_parameter = simses_parameter
        self.__level = file.level

    def _setup_config(self) -> dict:
        filefolder = os.getcwd() + "/Monte_Carlo_Profiles/Level" + str(self.__level)
        allfile_list = [f for f in listdir(filefolder) if isfile(join(filefolder, f))]
        if os.path.isfile(filefolder + '\SBAP_PV_EEN_Power_Munich_2014.csv.gz'):
            allfile_list.remove('SBAP_PV_EEN_Power_Munich_2014.csv.gz') # Get rid of PV profile

        max_power = 1000 * float(self.__simses_parameter["max_power"])
        capacity = 1000 * float(self.__simses_parameter["capacity"])
        cell_type = self.__simses_parameter["battery"]
        start_soc = str(float(self.__simses_parameter["start_soc"]) / 100)
        resolution = self.__simses_parameter["resolution"]
        peak_shaving_limit = int(self.__simses_parameter["peak_shaving_limit"]) * 1000
        pv_power = float(self.__simses_parameter["pv_power"]) * 1000  # in W

        DATE_FORMATS: [str] = ['%Y-%m-%d %H:%M:%S', '%Y-%m-%d %H:%M', '%d.%m.%Y %H:%M:%S', '%d.%m.%Y %H:%M',
                                      'epoch']
        loadprofile = pd.read_csv(self.__file.name)
        if len(loadprofile.columns) == 2:
            loadprofile = loadprofile.set_index(loadprofile.columns[0])

        time = loadprofile.index[1]
        date_format_known = 0
        for date_format in DATE_FORMATS:
            try:
                if date_format == 'epoch':
                    float(time)
                    timeindex = pd.to_datetime(loadprofile.index, unit='s')
                    date_format_known = 1
                else:
                    pd.to_datetime(time, format=date_format)
                    timeindex= pd.to_datetime(loadprofile.index, format=date_format)
                    date_format_known = 1
            except ValueError:
                pass

        if date_format_known == 0:
            raise Exception('Unknown date format for ' + time)

        config_generator: SimulationConfigGenerator = SimulationConfigGenerator()
        config_generator.load_default_config()

        config_generator.set_simulation_time()
        config_generator.set_simulation_time(str(timeindex[0]), str(timeindex[-1]), str(resolution), str(1) ) # last 1 is loop

        # EMS
        if self.__simses_parameter["ems"] == "ParallelMultiUse":
            config_generator.set_peak_shaving_strategy('PeakShaving', peak_shaving_limit)
            config_generator.set_operation_strategy("ParallelMultiUse")
            config_generator.set_multi_use_operation_strategy('SimplePeakShaving, ResidentialPvGreedy',
                                                              '0.5, 0.5', '0.5, 0.5', '1,2')
        elif self.__simses_parameter["ems"] == "SimplePeakShaving":
            config_generator.set_peak_shaving_strategy('SimplePeakShaving', peak_shaving_limit)
        elif self.__simses_parameter["ems"] == "ResidentialPvGreedy":
            config_generator.set_operation_strategy('ResidentialPvGreedy')

        # Storage System
        storage_1: str = config_generator.add_lithium_ion_battery(capacity, cell_type, float(start_soc), start_soh=1)
        dcdc_1: str = config_generator.add_no_loss_dcdc()
        acdc_1: str = config_generator.add_no_loss_acdc()
        housing_1: str = config_generator.add_no_housing()
        hvac_1: str = config_generator.add_no_hvac()
        ac_system_1: str = config_generator.add_storage_system_ac(max_power, 600, acdc_1, housing_1, hvac_1)
        config_generator.set_battery(start_soc)
        config_generator.clear_storage_system_ac()
        config_generator.add_storage_system_ac(max_power, 600, 'notton', 'no_housing', 'no_hvac')
        config_generator.clear_storage_system_dc()
        config_generator.add_storage_system_dc('ac_system_1', dcdc_1, storage_1)

        # Profile Config:
        config_generator.set_generation_profile_scaling('True')
        config_generator.set_load_generation_scaling_factor(str(pv_power))
        config_generator.set_load_profile_scaling('False')

        config_generator.set_profile_directory(filefolder)

        config_set = {}
        for filename in allfile_list:

            config_generator.set_profile_config(filename)
            config_set[filename] = config_generator.get_config()

        return config_set

    def _analysis_config(self) -> ConfigParser:
        config_generator: AnalysisConfigGenerator = AnalysisConfigGenerator()
        config_generator.print_results(False)
        config_generator.do_plotting(False)
        config_generator.merge_analysis(False)
        config_generator.do_batch_analysis(True)
        return config_generator.get_config()

    def clean_up(self) -> None:
        pass
